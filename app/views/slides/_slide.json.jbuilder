json.extract! slide, :id, :name, :created_at, :updated_at , :picture
json.url slide_url(slide, format: :json)
